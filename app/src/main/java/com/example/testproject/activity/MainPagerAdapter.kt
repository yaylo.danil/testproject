package com.example.testproject.activity

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.databinding.PageBinding

class MainPagerAdapter(
    private val swipeToPage: (Int) -> Unit,
    private val showNotification: (Int) -> Unit,
    private val cancelNotifications: (Int) -> Unit
) :
    RecyclerView.Adapter<MainPagerAdapter.Holder>() {
    private var pagesCount = 1

    class Holder(private val binding: PageBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            add: () -> Unit,
            delete: () -> Unit,
            showNotification: (Int) -> Unit
        ) {
            binding.pageNumber.text = (adapterPosition + 1).toString()
            binding.plusPageButton.setOnClickListener {
                add()
            }
            binding.minusPageButton.setOnClickListener {
                delete()
            }
            binding.createNotificationButton.setOnClickListener {
                showNotification(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            PageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(::addPage, ::deletePage, showNotification)
    }

    override fun getItemCount() = pagesCount

    private fun addPage() {
        pagesCount++
        notifyItemInserted(pagesCount - 1)
        swipeToPage(pagesCount - 1) //will move user to the last page
    }

    private fun deletePage() {
        if (pagesCount != 1) {
            pagesCount--
            cancelNotifications(pagesCount)
            notifyItemRemoved(pagesCount)
            swipeToPage(pagesCount) //will move user to the last page
        }
    }

    fun setPagesCount(pagesCount: Int) {
        this.pagesCount = pagesCount
        notifyDataSetChanged()
    }
}
