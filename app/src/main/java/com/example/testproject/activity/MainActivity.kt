package com.example.testproject.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testproject.databinding.ActivityMainBinding
import com.example.testproject.notification.NotificationManager

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var mainPagerAdapter: MainPagerAdapter
    private val viewModel by lazy { MainViewModel(NotificationManager(this)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViewPager()
    }

    override fun onResume() {
        super.onResume()
        checkSavedPagesCount()
        checkIfNeedToSwipe()
    }

    override fun onStop() {
        savePagesCount()
        super.onStop()
    }

    private fun initViewPager() {
        mainPagerAdapter = MainPagerAdapter(
            ::swipeToPage,
            viewModel::showNotification,
            viewModel::cancelNotifications
        )
        binding.mainViewPager.adapter = mainPagerAdapter
    }

    private fun checkIfNeedToSwipe() {
        val pageNumber = intent.getIntExtra(PAGE_NUMBER, 0)
        if (pageNumber != 0) {
            swipeToPage(pageNumber)
        }
    }

    private fun swipeToPage(position: Int) {
        binding.mainViewPager.setCurrentItem(position, true)
    }


    private fun savePagesCount() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putInt(PAGE_COUNT, mainPagerAdapter.itemCount)
            apply()
        }
    }

    private fun checkSavedPagesCount() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        mainPagerAdapter.setPagesCount(sharedPref.getInt(PAGE_COUNT, 1))
    }

    companion object {
        const val PAGE_NUMBER = "pageNumber"
        const val PAGE_COUNT = "pageCount"
    }
}
