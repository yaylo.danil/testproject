package com.example.testproject.activity

import androidx.lifecycle.ViewModel
import com.example.testproject.notification.NotificationManager

class MainViewModel(private val notificationManager: NotificationManager): ViewModel() {
    fun showNotification(pageNumber: Int) {
        notificationManager.showNotification(pageNumber)
    }

    fun cancelNotifications(pageNumber: Int) {
        notificationManager.cancelNotification(pageNumber)
    }
}
